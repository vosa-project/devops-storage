#!/bin/bash
# Script for checking DEVOPS Storage LVM lab objectives
# Objective name - Create a backup copy of the snapshot

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 30.03.2017
# Version - 0.0.1

LC_ALL=C

# START
# LVM_SNSH_COPY

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/lvm_snapshot_backup"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=lvm_snapshot_backup


}

# User interaction:  Create a backup copy of the snapshot to '/var/backup/copies/webserver_date_time.tar.gz' (retrieve date and time from snapshot) - check for file beforesnapshot.txt

LVM_SNSH_COPY () {

	while true
	do

   	# Check if a copy of a snapshot contains the file "beforesnapshot.txt" including text "Created before backup"
    	ssh root@$IP_to_SSH 'zgrep -a "Created before backup" /var/backup/copies/$(lvs | grep webserver_ | cut -f3 -d" ").tar.gz | grep beforesnapshot.txt'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nBackup copy of the snapshot of 'webserver' exists! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Backup copy of the snapshot of 'webserver' has not been created correctly!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

LVM_SNSH_COPY

exit 0
