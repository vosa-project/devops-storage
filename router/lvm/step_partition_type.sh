#!/bin/bash
# Script for checking DEVOPS Storage LVM lab objectives
# Objective name - Change partition type from '8e Linux LVM' to default '83 Linux' or delete partitions

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 30.03.2017
# Version - 0.0.1

LC_ALL=C

# START
# LVM_PARTITION_TYPE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/lvm_partition_type"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=lvm_partition_type


}

# User interaction: Change partition type from '8e Linux LVM' to default '83 Linux' or delete partitions sdb1, sdc1, sdd1, sde1

LVM_PARTITION_TYPE () {

	while true
	do

   	# Check if all partitions used in the lab are of type '83 Linux' or have been deleted  
    	ssh root@$IP_to_SSH 'fdisk -l /dev/sd[b-e] | grep "^/dev/" | grep -vc "83 Linux" | grep -x 0'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nLinux LVM partitions have been reset to default type or removed!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Some partitions on sdb, sdc, sdd, sde still have type '8e Linux LVM' or were incorrectly reset to default!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

LVM_PARTITION_TYPE

exit 0

