#!/bin/bash
# Script for checking DEVOPS Storage LVM lab objectives
# Objective name - Resize logical volume 'database'

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 30.03.2017
# Version - 0.0.1

LC_ALL=C

# START
# LVM_RESIZE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/lvm_resize_database"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=lvm_resize_database


}

# User interaction: Reduce the size of logical volume "database" by 6 GiB (new size 6 GiB)

LVM_RESIZE () {

	while true
	do

   	# Check if the size of lv "database" has been reduced to 6 GiB
    	ssh root@$IP_to_SSH 'lsblk | grep "data-database" | grep 6G'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nLogical volume 'database' has been resized to 6 GiB!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Logical volume 'database' has not been resized to 6 GiB! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

LVM_RESIZE

exit 0

