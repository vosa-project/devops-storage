#!/bin/bash

# description "Linear list of Devops Storage LVM lab checks for startchecks.conf"

# "general reusable checks"
/root/devops-storage/router/reusable/ssh_connection.sh || true
/root/devops-storage/router/reusable/sudo_privileges.sh || true

# "lab specific checks"
# /root/devops-storage/router/lvm/___.sh || true
/root/devops-storage/router/lvm/step_diskprep.sh || true
/root/devops-storage/router/lvm/step_pvcreate.sh || true
/root/devops-storage/router/lvm/step_vgcreate_data.sh || true
/root/devops-storage/router/lvm/step_lvcreate_database.sh || true
/root/devops-storage/router/lvm/step_mkfs_database.sh || true
/root/devops-storage/router/lvm/step_lvextend_database.sh || true
/root/devops-storage/router/lvm/step_resize_database.sh || true
/root/devops-storage/router/lvm/step_lvcreate_webserver.sh || true
#/root/devops-storage/router/lvm/step_mount_volumes.sh || true
/root/devops-storage/router/lvm/step_fstab.sh || true
/root/devops-storage/router/lvm/step_lvcreate_backup_copies.sh || true
/root/devops-storage/router/lvm/step_testfile.sh || true
/root/devops-storage/router/lvm/step_snapshot.sh || true
/root/devops-storage/router/lvm/step_mount_snapshot.sh || true
/root/devops-storage/router/lvm/step_snapshot_backup.sh || true
/root/devops-storage/router/lvm/step_snapshot_remove.sh || true
/root/devops-storage/router/lvm/step_lvremove.sh || true
/root/devops-storage/router/lvm/step_vgremove.sh || true
/root/devops-storage/router/lvm/step_pvremove.sh || true
/root/devops-storage/router/lvm/step_partition_type.sh || true


