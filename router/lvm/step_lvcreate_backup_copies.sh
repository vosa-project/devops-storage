#!/bin/bash
# Script for checking DEVOPS Storage LVM lab objectives
# Objective name - Create logical volume 'copies', size 4 GiB, on volume group 'backup' using drive /dev/sde and mount to /var/backup/copies

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 30.03.2017
# Version - 0.0.1

LC_ALL=C

# START
# LVM_LVCREATE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/lvm_lvcreate_backup_copies"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=lvm_lvcreate_backup_copies


}

# User interaction: Create logical volume 'copies', size 4 GiB, on volume group 'backup' using drive /dev/sde and is mounted to /var/backup/copies

LVM_LVCREATE () {

	while true
	do

   	# Check if lv "copies" on vg "backup" has been created with size 4 GiB  and mounted to /var/backup/copies
    	ssh root@$IP_to_SSH 'lsblk | grep "backup-copies" | grep 4G | grep lvm | grep /var/backup/copies'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nLogical volume 'backup-copies' with size 4 GiB has been mounted!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Logical volume 'backup-copies' with size 4 GiB has not been created or mounted! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

LVM_LVCREATE

exit 0

