#!/bin/bash
# Script for checking DEVOPS Storage LVM lab objectives
# Objective name - Mount automatically volume 'database' to '/var/data/database' and volume 'webserver' to '/var/data/webserver' (add both to /etc/fstab)

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 30.03.2017
# Version - 0.0.1

LC_ALL=C

# START
# LVM_FSTAB

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/lvm_fstab"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=lvm_fstab


}

# User interaction:  Mount volume 'database' to '/var/data/database' and volume 'webserver' to '/var/data/webserver' in /etc/fstab

LVM_FSTAB () {

	while true
	do

   	# Check if volumes 'database' and 'webserver' are being mounted automatically
    	ssh root@$IP_to_SSH 'df -h | grep "data-database" | grep "/var/data/database" && df -h | grep "data-webserver" | grep "/var/data/webserver" && grep "/var/data/database" /etc/fstab | grep -v "#" && grep "/var/data/webserver" /etc/fstab | grep -v "#" && mount -an'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nLogical volumes 'database' and 'webserver' are mounted automatically!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Logical volumes 'database' and 'webserver' are not mounted automatically! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

LVM_FSTAB

exit 0

