#!/bin/bash
# Script for checking DEVOPS Storage LVM lab objectives
# Objective name - Remove logical volumes

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 12.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# LVM_LVREMOVE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/lvm_lvremove"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=lvm_lvremove


}

# User interaction: Remove logical volumes on volume groups "data" and "backup" ("database", "webserver", "copies")

LVM_LVREMOVE () {

	while true
	do

   	# Check if logical volumes on volume groups "data" and "backup" have been removed 
    	ssh root@$IP_to_SSH 'lvscan | grep -ce "data\|backup" | grep -x 0 && mount -an'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nAll logical volumes on volume groups 'data' and 'backup' have been removed!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "All logical volumes created in the lab ('data-database', 'data-webserver', 'backup-copies') have not been removed or configurations deleted!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

LVM_LVREMOVE

exit 0

