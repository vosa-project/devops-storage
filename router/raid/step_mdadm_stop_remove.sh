#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Stop and remove all disks from RAID array /dev/md127

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# MDADM_STOP_REMOVE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/mdadm_stop_remove"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=mdadm_stop_remove

}

# User interaction: Stop and remove all disks from RAID array /dev/md127

MDADM_STOP_REMOVE () {

	while true
	do

   	# Check if RAID array /dev/md127 has been stopped and disks cleared
    	ssh root@$IP_to_SSH 'grep -wc "md127" /proc/mdstat | grep -x 0 && mdadm --query /dev/sd[b-e] /dev/sd[b-e][1-4] | grep -c device | grep -x 0'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nRAID array /dev/md127 has been stopped and disks cleared! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "RAID array /dev/md127 has not been completely removed!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MDADM_STOP_REMOVE

exit 0

