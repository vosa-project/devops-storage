#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Create ext4 filesystem on RAID array /dev/md127

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# RAID_MKFS

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/raid_mkfs"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=raid_mkfs


}

# User interaction: Create ext4 file system on RAID array /dev/md127

RAID_MKFS () {

	while true
	do

   	# Check if ext4 file system is on RAID array /dev/md127
    	ssh root@$IP_to_SSH 'lsblk -f | grep -w "md127" | grep -w "ext4"'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nRAID array /dev/md127 has ext4 file system! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "RAID array /dev/md127 does not have ext4 filesystem!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

RAID_MKFS

exit 0

