#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Force a disk to fail to be replaced by a spare disk on RAID-1 array /dev/md127

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# MDADM_FAIL_SPARE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/mdadm_fail_spare"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
    	set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=mdadm_fail_spare


}

# User interaction: Force a disk to fail to be replaced by a spare disk on RAID-1 array /dev/md127

MDADM_FAIL_SPARE () {

	while true
	do

   	# Check if a spare disk has replaced a failed disk on RAID array /dev/md127 
    	ssh root@$IP_to_SSH 'mdadm --detail /dev/md127 | grep -w "Working Devices : 2" && mdadm --detail /dev/md127 | grep -w "Failed Devices : 1"'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nA spare disk has replaced a failed disk on /dev/md127! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "A spare disk has not replaced a failed disk on /dev/md127!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MDADM_FAIL_SPARE

exit 0

