#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Delete RAID information from /etc/mdadm/mdadm.conf /etc/fstab

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# MDADM_CONF_DELETE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/mdadm_conf_delete"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

	# Exit if there are undeclared variables
	set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=mdadm_conf_delete

}

# User interaction: Delete RAID information from /etc/mdadm/mdadm.conf /etc/fstab

MDADM_CONF_DELETE () {

	while true
	do

   	# Check if RAID configuration has been deleted
    	ssh root@$IP_to_SSH 'grep -w "server:127" /etc/mdadm/mdadm.conf | grep -vc "#" | grep -x 0 && grep "/mnt/lab/raid" /etc/fstab | grep -vc "#"| grep -x 0'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nRAID configuration has been deleted! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "RAID configuration has not been deleted from either /etc/mdadm/mdadm.conf or /etc/fstab!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MDADM_CONF_DELETE

exit 0

