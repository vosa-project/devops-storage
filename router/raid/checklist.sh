#!/bin/bash

# description "Linear list of Devops Storage RAID lab checks for startchecks.conf"

# "general reusable checks"
/root/devops-storage/router/reusable/ssh_connection.sh || true
/root/devops-storage/router/reusable/sudo_privileges.sh || true

# "lab specific checks"
# /root/devops-storage/router/raid/___.sh || true
/root/devops-storage/router/raid/step_mdadm_create.sh || true
/root/devops-storage/router/raid/step_mdadm_conf.sh || true
/root/devops-storage/router/raid/step_mdadm_stop.sh || true
/root/devops-storage/router/raid/step_mdadm_assemble.sh || true
/root/devops-storage/router/raid/step_raid_mkfs.sh || true
/root/devops-storage/router/raid/step_raid_mount.sh || true
/root/devops-storage/router/raid/step_raid_testfile.sh || true
/root/devops-storage/router/raid/step_mdadm_fail_raid1.sh || true
/root/devops-storage/router/raid/step_mdadm_remove_add.sh || true
/root/devops-storage/router/raid/step_mdadm_add_spare.sh || true
/root/devops-storage/router/raid/step_mdadm_fail_spare.sh || true
/root/devops-storage/router/raid/step_mdadm_grow_level.sh || true
/root/devops-storage/router/raid/step_mdadm_grow_devices.sh || true
/root/devops-storage/router/raid/step_mdadm_add_device.sh || true
/root/devops-storage/router/raid/step_mdadm_fail_raid5.sh || true
/root/devops-storage/router/raid/step_mdadm_fail_group.sh || true
/root/devops-storage/router/raid/step_mdadm_stop_remove.sh || true
/root/devops-storage/router/raid/step_mdadm_conf_delete.sh || true

