#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Grow number of active devices in RAID-5 array /dev/md127 to four and expand file system

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# MDADM_ADD_DEVICE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/mdadm_add_device"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=mdadm_add_device


}

# User interaction: Grow number of active devices in RAID-5 array /dev/md127 to four and expand file system

MDADM_ADD_DEVICE () {

	while true
	do

   	# Check if the number of devices in RAID-5 array /dev/md127 is now four and file system has been expanded
    	ssh root@$IP_to_SSH 'mdadm --brief /dev/md127 | grep "raid5 4 devices" && df -h | grep /dev/md127 | grep 12G'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nRAID-5 group /dev/md127 has expanded to four active devices! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "RAID-5 array /dev/md127 has not expanded to four active devices!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MDADM_ADD_DEVICE

exit 0

