#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Create file test.txt with line "It works!" on RAID array /dev/md127

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# RAID_TESTFILE

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/raid_testfile"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=raid_testfile


}

# User interaction: Create file test.txt with line "It works!" on RAID array /dev/md127

RAID_TESTFILE () {

	while true
	do

   	# Check if file test.txt with text "It works!" has been created on /dev/md127
    	ssh root@$IP_to_SSH 'grep -iRl "It works" $(findmnt -nr -o target -S /dev/md127) | grep "test.txt"'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nTest file has been created on mounted RAID array md127! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "No file test.txt containing text 'It works!' has been created on /dev/md127!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

RAID_TESTFILE

exit 0

