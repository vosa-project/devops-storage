#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Mount RAID array /dev/md127 to /mnt/lab/raid

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# RAID_MOUNT

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/raid_mount"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=raid_mount


}

# User interaction: Mount RAID array /dev/md127 to /mnt/lab/raid

RAID_MOUNT () {

	while true
	do

   	# Check if RAID array /dev/md127 has been mounted to /mnt/lab/raid
    	ssh root@$IP_to_SSH 'df -T | grep -w /dev/md127 | grep -w ext4 | grep -w /mnt/lab/raid'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nRAID array /dev/md127 has been mounted! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "RAID array /dev/md127 has not been mounted to /mnt/lab/raid!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

RAID_MOUNT

exit 0

