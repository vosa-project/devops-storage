#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Convert /dev/md127 from RAID-1 to RAID-5 array

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 11.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# MDADM_GROW_LEVEL

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/mdadm_grow_level"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=mdadm_grow_level


}

# User interaction: Convert /dev/md127 from RAID-1 to RAID-5 array

MDADM_GROW_LEVEL () {

	while true
	do

   	# Check if /dev/md127 has been converted to RAID-5 array
    	ssh root@$IP_to_SSH 'grep "md127 : active raid5" /proc/mdstat'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nRAID-5 group /dev/md127 is now active with two devices! Date: `date`\n" && touch $CheckFile
		$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "RAID-5 array /dev/md127 does not exist yet!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MDADM_GROW_LEVEL

exit 0

