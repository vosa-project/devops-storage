#!/bin/bash
# Script for checking DEVOPS Storage ... lab objectives
# Objective name - Usertest - creating user (test) (test)

# Author - Katrin Loodus
# Modified by - Roland Kaur
# Modified by - Oliver Tiks

# Created @ 27.04.2016
# Modified @ 30.03.2017
# Version - 0.0.2

LC_ALL=C

# START
# USERTEST

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/usertest"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

	# IP to SSH to - devops storage lab server (192.168.0.2)
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=usertest


}

# User interaction: Create new user 'test'

USERTEST () {

	while true
	do

   	# Check if user 'test' has been created 
    	ssh root@$IP_to_SSH 'getent passwd test'  

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nUser 'test' has been created!! Date: `date`\n" && touch $CheckFile
        	$DIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $DIR/objectiveschecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "User 'test' has not been created! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

USERTEST

exit 0

