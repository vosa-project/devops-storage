#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Create extended partition on /dev/sdb

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 10.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# MBR_EXTENDED

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/mbr_extended"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=mbr_extended


}

# User interaction: Create extended partition on /dev/sdb

MBR_EXTENDED () {

	while true
	do

   	# Check if extended partition has been created on /dev/sdb
    	ssh root@$IP_to_SSH 'fdisk -l /dev/sdb | grep -w 3.3G | grep Extended'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nExtended partition has been created on /dev/sdb!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Extended partition has not been created on /dev/sdb!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MBR_EXTENDED

exit 0

