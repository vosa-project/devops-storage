#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Create two logical partitions on /dev/sdb

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 10.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# MBR_LOGICAL

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/mbr_logical"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=mbr_logical


}

# User interaction: Create two logical partitions on /dev/sdb

MBR_LOGICAL () {

	while true
	do

   	# Check if two logical partitions have been created on /dev/sdb
    	ssh root@$IP_to_SSH 'fdisk -l /dev/sdb | grep -w sdb5 | grep -w 1G && fdisk -l /dev/sdb | grep -w sdb6 | grep -w 2.3G'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nTwo logical partitions have been created on /dev/sdb!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Two logical partitions have not been created on /dev/sdb!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

MBR_LOGICAL

exit 0

