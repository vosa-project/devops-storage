#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Make swap area on /dev/sdc1 and turn it on automatically

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 10.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# GPT_MKSWAP

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/gpt_mkswap"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=gpt_mkswap


}

# User interaction:  Make swap area on /dev/sdc1 and turn it on automatically

GPT_MKSWAP () {

	while true
	do

   	# Check if swap area /dev/sdc1 is turned on and in fstab
    	ssh root@$IP_to_SSH 'fdisk -l /dev/sdc | grep -w sdc1 | grep -w 750M && grep -w "$(lsblk -no UUID /dev/sdc1)\|/dev/sdc1" /etc/fstab | grep -w "sw" | grep -v "#" && swapon -s | grep -w /dev/sdc1 && swapon -a'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nSwap area /dev/sdc1 is turned on automatically!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Swap area /dev/sdc1 is not turned on automatically!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

GPT_MKSWAP

exit 0

