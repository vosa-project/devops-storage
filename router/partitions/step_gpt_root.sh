#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name -  Create ext4 file system on /dev/sdc2 and mount it automatically

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 10.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# GPT_ROOT

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/gpt_root"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=gpt_root


}

# User interaction: Create ext4 file system on /dev/sdc2 and mount it automatically to /mnt/lab/gpt/system

GPT_ROOT () {

	while true
	do

   	# Check if /dev/sdc2 has ext4 filesystem and is mounted automatically
    	ssh root@$IP_to_SSH 'df -T | grep /dev/sdc2 | grep ext4 | grep /mnt/lab/gpt/system && grep -w "$(lsblk -no UUID /dev/sdc2)\|/dev/sdc2" /etc/fstab | grep -w /mnt/lab/gpt/system | grep -v "#" && mount -an'


   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nPartition /dev/sdc2 has ext4 file system and is mounted automatically to /mnt/lab/gpt/system! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Partition /dev/sdc2 is not mounted correctly or does not have ext4 file system!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

GPT_ROOT

exit 0

