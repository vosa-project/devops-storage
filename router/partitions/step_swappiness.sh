#!/bin/bash
# Script for checking DEVOPS Storage Partitions lab objectives
# Objective name - Decrease swap usage to 10 permanently

# Template author - Katrin Loodus
# Modified by Roland Kaur
# Current version by Oliver Tiks

# Date - 10.04.2017
# Version - 0.0.1

LC_ALL=C

# START
# SWAPPINESS

# Set variables

START () {

	# Enable logging
	echo -e "\n$0 started on: $(date):" >> /var/log/labcheckslog.log
	exec &> >(tee -a /var/log/labcheckslog.log)

	# If $CheckFile exists, then exit the script
	CheckFile="/var/labchecks/swappiness"

	if [ -f $CheckFile ]; then echo "$0 has already ran successfully" && exit 0; fi

    # Exit if there are undeclared variables
    set -o nounset     

	# Get working directory
	DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

        # Get parent directory
        # PARENTDIR=$( cd "$( dirname "$DIR" )" && pwd )
       	PARENTDIR="$(dirname "$DIR")"

	# IP to SSH to - storage lab server 192.168.0.2
	IP_to_SSH=labserver

	# Time to sleep between running the check again
	Sleep=5

	# Objective uname in VirtualTA
	Uname=swappiness


}

# User interaction: Decrease swap usage to 10 permanently

SWAPPINESS () {

	while true
	do

   	# Check if swappiness parameter is set to 10 permanently
    	ssh root@$IP_to_SSH 'cat /proc/sys/vm/swappiness | grep -x 10 && grep vm.swappiness /etc/sysctl.conf  | grep -v "#" | grep -w 10'

   	# Run objectiveschecks.py and update VirtualTa with correct value
    	if [ $? -eq 0 ]; then

        	echo -e "\nSwappiness parameter is set to 10 permanently!! Date: `date`\n" && touch $CheckFile
        	$PARENTDIR/objectivechecks.py $Uname True || echo -e "\nFailed to run $PARENTDIR/objectivechecks.py! Date: `date`" >&2 && exit 1
        	exit 0

    	else

        	echo -e "Swappiness parameter has not been set to 10 permanently!!!! Date: `date`\n" >&2
        	sleep $Sleep

    	fi
	done

}

START

SWAPPINESS

exit 0

