#!/bin/bash

# description "Linear list of Devops Storage Partitions lab checks for startchecks.conf"

# "general reusable checks"
/root/devops-storage/router/reusable/ssh_connection.sh || true
/root/devops-storage/router/reusable/sudo_privileges.sh || true

# "lab specific checks"
# /root/devops-storage/router/partitions/___.sh || true
/root/devops-storage/router/partitions/step_mbr_table.sh || true
/root/devops-storage/router/partitions/step_mbr_mkswap.sh || true
/root/devops-storage/router/partitions/step_swappiness.sh || true
/root/devops-storage/router/partitions/step_mbr_extended.sh || true
/root/devops-storage/router/partitions/step_mbr_logical.sh || true
/root/devops-storage/router/partitions/step_mbr_root.sh || true
/root/devops-storage/router/partitions/step_mbr_home.sh || true
/root/devops-storage/router/partitions/step_gpt_table.sh || true
/root/devops-storage/router/partitions/step_gpt_mkswap.sh || true
/root/devops-storage/router/partitions/step_gpt_partitions.sh || true
/root/devops-storage/router/partitions/step_gpt_root.sh || true
/root/devops-storage/router/partitions/step_gpt_home.sh || true

